<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress-demo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Wonder@2016');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eL2_sVW+6Nqu_#wqO(g7ixE4Us`~^J(!l*=Gk0M4~:a!q)JYPOj$3xB,zGQvN`ye');
define('SECURE_AUTH_KEY',  'pv(6RPY&3V)r^sXD#FIyh[F}!=y0BlA8Vh0zct4 me6]gw&t={^SYGpQ,dghYWZo');
define('LOGGED_IN_KEY',    ']$Z1_O-N<%m>SX07Ei-v)]%2i>A~yZF`cs:vYz<2TAcBAj1@%PoqxQ2LOvp8Vl3@');
define('NONCE_KEY',        '!?Iw^K6-nA11V4;x{>S,f@D;^3h>5~p)N{QxI,%Kl$x$SH,0:2Sx)}]C_Y!Z{C|W');
define('AUTH_SALT',        'av??}8hbKMZIvI!Lh$Udb[6V[]xa3v8%|E}-cP^/$L%Un4Ap+5DE?)bJ.#77()@x');
define('SECURE_AUTH_SALT', '(ppip@B-;]l#WT_@w:|^P|N0<Zh/F6a;-Y4Bp1M9z~i^ay^A?@NV~3dqBWi!M6e(');
define('LOGGED_IN_SALT',   '(L<%u/&{nXlUzMZm2dK=- /<%U80wy$*j6vGQ7d?h7F&_QlZxI=[{4sOht|pQ.87');
define('NONCE_SALT',       '@/oZ(%-09dU!k)uql=Z_E9`_22lYP:55R#Mz!b5QWJB[PIX[F$jN=|}:YPhaGQZU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
